<?php
$graph = new Graph();

$node_a = new Vertex("A");
$node_b = new Vertex("B");
$node_c = new Vertex("C");
$node_d = new Vertex("D");
$node_e = new Vertex("E");
$node_f = new Vertex("F");
$node_g = new Vertex("G");
$node_h = new Vertex("H");
$node_i = new Vertex("I");

// Distance = Time
$node_a->connect($node_c, 1);
$node_a->connect($node_h, 10);
$node_a->connect($node_e, 30);

$node_b->connect($node_c, 1);
$node_b->connect($node_i, 65);
$node_b->connect($node_g, 64);

$node_c->connect($node_a, 1);
$node_c->connect($node_b, 1);

$node_d->connect($node_f, 4);
$node_d->connect($node_e, 3);

$node_e->connect($node_a, 30);
$node_e->connect($node_h, 30);
$node_e->connect($node_d, 3);

$node_f->connect($node_d, 4);
$node_f->connect($node_i, 45);
$node_f->connect($node_g, 40);

$node_g->connect($node_f, 40);
$node_g->connect($node_b, 64);

$node_h->connect($node_a, 10);
$node_h->connect($node_e, 30);

$node_i->connect($node_b, 65);
$node_i->connect($node_f, 45);

$graph->add($node_a);
$graph->add($node_b);
$graph->add($node_c);
$graph->add($node_d);
$graph->add($node_e);
$graph->add($node_f);
$graph->add($node_g);
$graph->add($node_h);
$graph->add($node_i);

$algorithm = new Algorithm\Dijkstra($graph);
$algorithm->setStartingVertex($node_a);
$algorithm->setEndingVertex($node_d);

echo $algorithm->getLiteralShortestPath() . ": distance " . $algorithm->getDistance();