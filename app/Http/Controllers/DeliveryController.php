<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Doctrine\OrientDB\Graph\Graph;
use Doctrine\OrientDB\Graph\Vertex;
use Doctrine\OrientDB\Graph\Algorithm;

class DeliveryController extends Controller
{

    private $graph, $node_a, $node_b, $node_c, $node_d, $node_e, $node_f, $node_g, $node_h, $node_i;

    public function __construct()
    {
        $this->graph = new Graph();

        $this->node_a = new Vertex("A");
        $this->node_b = new Vertex("B");
        $this->node_c = new Vertex("C");
        $this->node_d = new Vertex("D");
        $this->node_e = new Vertex("E");
        $this->node_f = new Vertex("F");
        $this->node_g = new Vertex("G");
        $this->node_h = new Vertex("H");
        $this->node_i = new Vertex("I");

        // Assume That Time = Distance
        $this->node_a->connect($this->node_c, 1);
        $this->node_a->connect($this->node_h, 10);
        $this->node_a->connect($this->node_e, 30);

        $this->node_b->connect($this->node_c, 1);
        $this->node_b->connect($this->node_i, 65);
        $this->node_b->connect($this->node_g, 64);

        $this->node_c->connect($this->node_a, 1);
        $this->node_c->connect($this->node_b, 1);

        $this->node_d->connect($this->node_f, 4);
        $this->node_d->connect($this->node_e, 3);

        $this->node_e->connect($this->node_a, 30);
        $this->node_e->connect($this->node_h, 30);
        $this->node_e->connect($this->node_d, 3);

        $this->node_f->connect($this->node_d, 4);
        $this->node_f->connect($this->node_i, 45);
        $this->node_f->connect($this->node_g, 40);

        $this->node_g->connect($this->node_f, 40);
        $this->node_g->connect($this->node_b, 64);

        $this->node_h->connect($this->node_a, 10);
        $this->node_h->connect($this->node_e, 30);

        $this->node_i->connect($this->node_b, 65);
        $this->node_i->connect($this->node_f, 45);

        $this->graph->add($this->node_a);
        $this->graph->add($this->node_b);
        $this->graph->add($this->node_c);
        $this->graph->add($this->node_d);
        $this->graph->add($this->node_e);
        $this->graph->add($this->node_f);
        $this->graph->add($this->node_g);
        $this->graph->add($this->node_h);
        $this->graph->add($this->node_i);

    }

    public function processDelivery(Request $request)
    {
        $algorithm = new Algorithm\Dijkstra($this->graph);

        $algorithm->setStartingVertex($this->node_a);
        $algorithm->setEndingVertex($this->node_g);

        echo $algorithm->getLiteralShortestPath() . ": distance " . $algorithm->getDistance();
    }
}